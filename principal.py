from requests_html import HTMLSession
from tkinter import *
from tkinter import messagebox
import random
import webbrowser


session = HTMLSession()

root = Tk()
root.title('Recettes Aléatoire')
root.geometry("1000x470")
root.configure(bg='lightblue')

def marmiton():
    session = HTMLSession()
    liste_ingredient = [nom.get(), prenom.get(), date_naissance.get(), adresse.get(), ville.get(), code_postal.get()]
    new_liste = ''
    for elem in liste_ingredient:
        if elem:
            new_liste = new_liste + elem + '-'
    new_liste = new_liste[:-1]
    r = session.get('https://www.marmiton.org/recettes/recherche.aspx?aqt=' + new_liste)
    aleatoire = random.randrange(1, 8)
    about = r.html.find('.MRTN__sc-1gofnyi-0')
    lelien = about[0].absolute_links
    lelien1 = list(lelien)
    lienfinal = lelien1[aleatoire]
    webbrowser.open(lienfinal,new=2)


def septcent():
    liste_ingredient = [nom.get(), prenom.get(), date_naissance.get(), adresse.get(), ville.get(), code_postal.get()]
    new_liste = ''
    for elem in liste_ingredient:
        if elem:
            new_liste = new_liste + elem + '+'
    session = HTMLSession()
    new_liste = new_liste[:-1]
    aleatoire = random.randrange(2, 14)
    aleatoire = str(aleatoire)
    r = session.get('https://www.750g.com/recherche/?q=' + new_liste)
    for elem in range(10):
        try:
            about = r.html.find('.card-listing > div:nth-child(' +aleatoire+ ') > article:nth-child(1) > div:nth-child(1) > div:nth-child(2) > strong:nth-child(1) > a:nth-child(1)', first=True)
            lelien = about.absolute_links
            lelien = list(lelien)
            lelien1 = lelien[0]
            lelien1 = str(lelien1)
            new = 2
            choice = lelien1
            site = choice
            webbrowser.open(site,new=new)
            break
        except AttributeError:
            continue

def cuisineaz():
    liste_ingredient = [nom.get(), prenom.get(), date_naissance.get(), adresse.get(), ville.get(), code_postal.get()]
    new_liste = ''
    for elem in liste_ingredient:
        if elem:
            new_liste = new_liste + elem + "+"
    session = HTMLSession()
    r = session.get("https://www.cuisineaz.com/recettes/recherche_terme.aspx?recherche="+ new_liste)
    aleatoire = random.randrange(1, 7)
    about = r.html.find('.medium-12')
    lelien = about[0].absolute_links
    lelien1 = list(lelien)
    print(lelien1)
    lienfinal = lelien1[aleatoire]
    webbrowser.open(lienfinal,new=2)

def journal():
    liste_ingredient = [nom.get(), prenom.get(), date_naissance.get(), adresse.get(), ville.get(), code_postal.get()]
    new_liste = ''
    for elem in liste_ingredient:
        if elem:
            new_liste = new_liste + elem + '+'
    new_liste = new_liste[:-1]
    session = HTMLSession()
    r = session.get("https://cuisine.journaldesfemmes.fr/s/?f_libelle=" + new_liste)
    for elem in range(10):
        try:
            aleatoire = random.randrange(4, 14)
            aleatoire = str(aleatoire)
            about = r.html.find('li.jSelectionItemsListItem:nth-child('+aleatoire+') > div:nth-child(1) > div:nth-child(2) > p:nth-child(1) > a:nth-child(1)', first=True)
            lelien = about.absolute_links
            lelien = list(lelien)
            lelien1 = lelien[0]
            lelien1 = str(lelien1)
            new = 2
            choice = lelien1
            site = choice
            webbrowser.open(site,new=new)
            break
        except AttributeError:
            continue

def infogre1():
    ingredient = nom.get()
    a = session.get('https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Fiche.aspx?doc='+ingredient+'_nu')
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        text_widget = Text(root2, height=800, width=800)
        scroll_bar = Scrollbar(root2)
        scroll_bar.pack(side=RIGHT)
        text_widget.pack(side=LEFT)
        about1 = a.html.find('article.pst20', first=True)
        liste = about1.text
        i = 1
        for elem in range(10):
            if 'Calories' not in liste and 'Nutriments' not in liste:
                about1 = a.html.find('#page_contenuFiche_col1 > div:nth-child('+str(i)+')', first=True)
                liste = about1.text
                i = i + 1
            if 'Calories' in liste or 'Nutriments' in liste:
                break
        text_widget.insert(END, liste)

    except AttributeError:        
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre2():
    ingredient = prenom.get()
    a = session.get('https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Fiche.aspx?doc='+ingredient+'_nu')
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        text_widget = Text(root2, height=800, width=800)
        scroll_bar = Scrollbar(root2)
        scroll_bar.pack(side=RIGHT)
        text_widget.pack(side=LEFT)
        about1 = a.html.find('article.pst20', first=True)
        liste = about1.text
        i = 1
        for elem in range(10):
            if 'Calories' not in liste and 'Nutriments' not in liste:
                about1 = a.html.find('#page_contenuFiche_col1 > div:nth-child('+str(i)+')', first=True)
                liste = about1.text
                i = i + 1
            if 'Calories' in liste or 'Nutriments' in liste:
                break
        text_widget.insert(END, liste)

    except AttributeError:        
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre3():
    ingredient = date_naissance.get()
    a = session.get('https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Fiche.aspx?doc='+ingredient+'_nu')
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        text_widget = Text(root2, height=800, width=800)
        scroll_bar = Scrollbar(root2)
        scroll_bar.pack(side=RIGHT)
        text_widget.pack(side=LEFT)
        about1 = a.html.find('article.pst20', first=True)
        liste = about1.text
        i = 1
        for elem in range(10):
            if 'Calories' not in liste and 'Nutriments' not in liste:
                about1 = a.html.find('#page_contenuFiche_col1 > div:nth-child('+str(i)+')', first=True)
                liste = about1.text
                i = i + 1
            if 'Calories' in liste or 'Nutriments' in liste:
                break
        text_widget.insert(END, liste)

    except AttributeError:        
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre4():
    ingredient = adresse.get()
    a = session.get('https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Fiche.aspx?doc='+ingredient+'_nu')
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        text_widget = Text(root2, height=800, width=800)
        scroll_bar = Scrollbar(root2)
        scroll_bar.pack(side=RIGHT)
        text_widget.pack(side=LEFT)
        about1 = a.html.find('article.pst20', first=True)
        liste = about1.text
        i = 1
        for elem in range(10):
            if 'Calories' not in liste and 'Nutriments' not in liste:
                about1 = a.html.find('#page_contenuFiche_col1 > div:nth-child('+str(i)+')', first=True)
                liste = about1.text
                i = i + 1
            if 'Calories' in liste or 'Nutriments' in liste:
                break
        text_widget.insert(END, liste)

    except AttributeError:        
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre5():
    ingredient = ville.get()
    a = session.get('https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Fiche.aspx?doc='+ingredient+'_nu')
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        text_widget = Text(root2, height=800, width=800)
        scroll_bar = Scrollbar(root2)
        scroll_bar.pack(side=RIGHT)
        text_widget.pack(side=LEFT)
        about1 = a.html.find('article.pst20', first=True)
        liste = about1.text
        i = 1
        for elem in range(10):
            if 'Calories' not in liste and 'Nutriments' not in liste:
                about1 = a.html.find('#page_contenuFiche_col1 > div:nth-child('+str(i)+')', first=True)
                liste = about1.text
                i = i + 1
            if 'Calories' in liste or 'Nutriments' in liste:
                break
        text_widget.insert(END, liste)

    except AttributeError:        
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre6():
    ingredient = code_postal.get()
    a = session.get('https://www.passeportsante.net/fr/Nutrition/EncyclopedieAliments/Fiche.aspx?doc='+ingredient+'_nu')
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        text_widget = Text(root2, height=800, width=800)
        scroll_bar = Scrollbar(root2)
        scroll_bar.pack(side=RIGHT)
        text_widget.pack(side=LEFT)
        about1 = a.html.find('article.pst20', first=True)
        liste = about1.text
        i = 1
        for elem in range(10):
            if 'Calories' not in liste and 'Nutriments' not in liste:
                about1 = a.html.find('#page_contenuFiche_col1 > div:nth-child('+str(i)+')', first=True)
                liste = about1.text
                i = i + 1
            if 'Calories' in liste or 'Nutriments' in liste:
                break
        text_widget.insert(END, liste)

    except AttributeError:        
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre11():
    ingredient = nom.get()
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        a = session.get('https://informationsnutritionnelles.fr/'+ingredient)
        about1 = a.html.find('#maininside', first=True)
        liste = about1.text
        text_widget = Text(root2, height=800, width=800)
        # Create a scrollbar
        scroll_bar = Scrollbar(root2)
        # Pack the scroll bar
        # Place it to the right side, using tk.RIGHT
        scroll_bar.pack(side=RIGHT)
        # Pack it into our tkinter application
        # Place the text widget to the left side
        text_widget.pack(side=LEFT)
       # Insert text into the text widget
        text_widget.insert(END, liste)
    except:
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre22():
    ingredient = prenom.get()
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        a = session.get('https://informationsnutritionnelles.fr/'+ingredient)
        about1 = a.html.find('#maininside', first=True)
        liste = about1.text
        text_widget = Text(root2, height=800, width=800)
        # Create a scrollbar
        scroll_bar = Scrollbar(root2)
        # Pack the scroll bar
        # Place it to the right side, using tk.RIGHT
        scroll_bar.pack(side=RIGHT)
        # Pack it into our tkinter application
        # Place the text widget to the left side
        text_widget.pack(side=LEFT)
       # Insert text into the text widget
        text_widget.insert(END, liste)
    except:
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre33():
    ingredient = date_naissance.get()
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        a = session.get('https://informationsnutritionnelles.fr/'+ingredient)
        about1 = a.html.find('#maininside', first=True)
        liste = about1.text
        text_widget = Text(root2, height=800, width=800)
        # Create a scrollbar
        scroll_bar = Scrollbar(root2)
        # Pack the scroll bar
        # Place it to the right side, using tk.RIGHT
        scroll_bar.pack(side=RIGHT)
        # Pack it into our tkinter application
        # Place the text widget to the left side
        text_widget.pack(side=LEFT)
       # Insert text into the text widget
        text_widget.insert(END, liste)
    except:
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre44():
    ingredient = adresse.get()
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        a = session.get('https://informationsnutritionnelles.fr/'+ingredient)
        about1 = a.html.find('#maininside', first=True)
        liste = about1.text
        text_widget = Text(root2, height=800, width=800)
        # Create a scrollbar
        scroll_bar = Scrollbar(root2)
        # Pack the scroll bar
        # Place it to the right side, using tk.RIGHT
        scroll_bar.pack(side=RIGHT)
        # Pack it into our tkinter application
        # Place the text widget to the left side
        text_widget.pack(side=LEFT)
       # Insert text into the text widget
        text_widget.insert(END, liste)
    except:
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre55():
    ingredient = ville.get()
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        a = session.get('https://informationsnutritionnelles.fr/'+ingredient)
        about1 = a.html.find('#maininside', first=True)
        liste = about1.text
        text_widget = Text(root2, height=800, width=800)
        # Create a scrollbar
        scroll_bar = Scrollbar(root2)
        # Pack the scroll bar
        # Place it to the right side, using tk.RIGHT
        scroll_bar.pack(side=RIGHT)
        # Pack it into our tkinter application
        # Place the text widget to the left side
        text_widget.pack(side=LEFT)
       # Insert text into the text widget
        text_widget.insert(END, liste)
    except:
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()

def infogre66():
    ingredient = code_postal.get()
    try:
        root2 = Tk()
        root2.title(ingredient)
        root2.geometry('800x800')
        a = session.get('https://informationsnutritionnelles.fr/'+ingredient)
        about1 = a.html.find('#maininside', first=True)
        liste = about1.text
        text_widget = Text(root2, height=800, width=800)
        # Create a scrollbar
        scroll_bar = Scrollbar(root2)
        # Pack the scroll bar
        # Place it to the right side, using tk.RIGHT
        scroll_bar.pack(side=RIGHT)
        # Pack it into our tkinter application
        # Place the text widget to the left side
        text_widget.pack(side=LEFT)
       # Insert text into the text widget
        text_widget.insert(END, liste)
    except:
        liste2 = 'Récuperation information impossible'
        reponse = messagebox.showinfo(ingredient, liste2)
        root2.destroy()
#create ingredients boxes
nom = Entry(root, width=30, borderwidth=2)
nom.grid(row=0, column=1, padx=20, pady=(10, 0))
prenom = Entry(root, width=30, borderwidth=2)
prenom.grid(row=1, column=1)
date_naissance = Entry(root, width=30, borderwidth=2)
date_naissance.grid(row=2, column=1)
adresse = Entry(root, width=30, borderwidth=2)
adresse.grid(row=3, column=1)
ville = Entry(root, width=30, borderwidth=2)
ville.grid(row=4, column=1)
code_postal = Entry(root, width=30, borderwidth=2)
code_postal.grid(row=5, column=1)

#create ingredients label
ingre1 = Label(root, text="ingrédient 1", borderwidth=2, relief="groove")
ingre1.grid(row=0, column=0, pady=(10, 0))
ingre2 = Label(root, text="ingrédient 2", borderwidth=2, relief="groove")
ingre2.grid(row=1, column=0)
ingre3 = Label(root, text="ingrédient 3", borderwidth=2, relief="groove")
ingre3.grid(row=2, column=0)
ingre4 = Label(root, text="ingrédient 4", borderwidth=2, relief="groove")
ingre4.grid(row=3, column=0)
ingre5 = Label(root, text="ingrédient 5", borderwidth=2, relief="groove")
ingre5.grid(row=4, column=0)
ingre6 = Label(root, text="ingrédient 6", borderwidth=2, relief="groove")
ingre6.grid(row=5, column=0)

#chercher marmiton boutton
submit_but = Button(root, text="Rechercher sur marmiton", borderwidth=2, relief="raised", command=marmiton)
submit_but.grid(row=9, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

#chercher 750g boutton
septcentcinquante = Button(root, text="Rechercher sur 750g", borderwidth=2, relief="raised", command=septcent)
septcentcinquante.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

#chercher cuisineAZ boutton
cuisine = Button(root, text="Rechercher sur cuisineAZ", borderwidth=2, relief="raised", command=cuisineaz)
cuisine.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

#chercher cuisineAZ boutton
journaldef = Button(root, text="Rechercher sur journal de femme", borderwidth=2, relief="raised", command=journal)
journaldef.grid(row=12, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

#creation bouton informations nutritionnel
ingredien1 = Button(root, text="<== information ingrédient 1", borderwidth=2, relief="raised", command=infogre1)
ingredien1.grid(row=0, column=2, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien2 = Button(root, text="<== information ingrédient 2", borderwidth=2, relief="raised", command=infogre2)
ingredien2.grid(row=1, column=2, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien3 = Button(root, text="<== information ingrédient 3", borderwidth=2, relief="raised", command=infogre3)
ingredien3.grid(row=2, column=2, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien4 = Button(root, text="<== information ingrédient 4", borderwidth=2, relief="raised", command=infogre4)
ingredien4.grid(row=3, column=2, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien5 = Button(root, text="<== information ingrédient 5", borderwidth=2, relief="raised", command=infogre5)
ingredien5.grid(row=4, column=2, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien6 = Button(root, text="<== information ingrédient 6", borderwidth=2, relief="raised", command=infogre6)
ingredien6.grid(row=5, column=2, columnspan=2, pady=5, padx=5, ipadx=20)

#creation bouton informations nutritionnel(bis)
ingredien11 = Button(root, text="infos ingrédient pour 100g", borderwidth=2, relief="raised", command=infogre11)
ingredien11.grid(row=0, column=4, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien22 = Button(root, text="infos ingrédient pour 100g", borderwidth=2, relief="raised", command=infogre22)
ingredien22.grid(row=1, column=4, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien33 = Button(root, text="infos ingrédient pour 100g", borderwidth=2, relief="raised", command=infogre33)
ingredien33.grid(row=2, column=4, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien44 = Button(root, text="infos ingrédient pour 100g", borderwidth=2, relief="raised", command=infogre44)
ingredien44.grid(row=3, column=4, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien55 = Button(root, text="infos ingrédient pour 100g", borderwidth=2, relief="raised", command=infogre55)
ingredien55.grid(row=4, column=4, columnspan=2, pady=5, padx=5, ipadx=20)
ingredien66 = Button(root, text="infos ingrédient pour 100g", borderwidth=2, relief="raised", command=infogre66)
ingredien66.grid(row=5, column=4, columnspan=2, pady=5, padx=5, ipadx=20)

if __name__ == "__main__":
    root.mainloop()
